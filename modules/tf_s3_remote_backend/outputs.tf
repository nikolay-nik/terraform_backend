output bucket {
  value = aws_s3_bucket.state_bucket
}

output dynamo_db {
  value = aws_dynamodb_table.state_locks
}