locals {
  bucket_name = "main-terraform-states"
}

resource "aws_s3_bucket" "state_bucket" {
  bucket = local.bucket_name

  versioning {
    enabled = true
  }
}

resource aws_dynamodb_table "state_locks" {
  name = "main_terraform-state-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    bucket = local.bucket_name
    key = "terraform.tfstate"
    workspace_key_prefix = var.workspace_prefix
    region = var.region

    dynamodb_table = aws_dynamodb_table.state_locks.name
    encrypt = var.encrypted
  }
}