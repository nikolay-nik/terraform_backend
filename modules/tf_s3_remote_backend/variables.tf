variable "workspace_prefix" {
  type = string
}

variable "region" {
  type = string
}

variable "encrypted" {
  type = bool
  default = true
}