locals {
  region = "eu-west-2"
}

provider aws {
  region = local.region
}

module remote_backend {
  source = "../../modules/tf_s3_remote_backend"
  workspace_prefix = "/terraform/test/s3_backend"
  region = local.region
  encrypted = false
}