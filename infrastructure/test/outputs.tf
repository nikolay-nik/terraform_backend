output s3_bucket_name {
  value = module.remote_backend.bucket.id
}

output "dynamodb_name" {
  value = module.remote_backend.dynamo_db.name
}